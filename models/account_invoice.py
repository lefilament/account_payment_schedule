# -*- coding: utf-8 -*-

# © 2018 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from dateutil.relativedelta import relativedelta
from odoo import api, fields, models


class AccountPaymentSchedule(models.Model):
    _inherit = "account.payment.term"

    payment_schedule = fields.Boolean(string="Créer un échéancier",)
    show_schedule = fields.Boolean(string="Afficher sur les factures",)
    number_term = fields.Integer(string="Nombre d'échéances", default=12)
    day_term = fields.Integer(string="Jour de l'échéance", default=20)
    interval_term = fields.Integer(string="Intervale (mois)", default=1)

    @api.one
    def compute(self, value, date_ref=False):
        date_ref = date_ref or fields.Date.today()
        amount = value
        sign = -1 if value < 0 else 1
        result = []
        if self.env.context.get('currency_id'):
            currency = self.env['res.currency'].browse(
                self.env.context['currency_id'])
        else:
            currency = self.env.user.company_id.currency_id
        prec = currency.decimal_places

        if self.payment_schedule:
            next_date = fields.Date.from_string(date_ref)
            paid = 0.0
            if next_date.day > self.day_term:
                next_date += relativedelta(day=self.day_term, months=1)
            else:
                next_date += relativedelta(day=self.day_term)

            for i in range(self.number_term - 1):
                result.append((next_date,
                               round(amount / self.number_term, prec)))
                next_date += relativedelta(months=+self.interval_term)
                paid += round(amount / self.number_term, prec)
            result.append((next_date, round(amount - paid, prec)))
        else:
            for line in self.line_ids:
                if line.value == 'fixed':
                    amt = sign * round(line.value_amount, prec)
                elif line.value == 'percent':
                    amt = round(value * (line.value_amount / 100.0), prec)
                elif line.value == 'balance':
                    amt = round(amount, prec)
                if amt:
                    next_date = fields.Date.from_string(date_ref)
                    if line.option == 'day_after_invoice_date':
                        next_date += relativedelta(days=line.days)
                    elif line.option == 'fix_day_following_month':
                        # Getting 1st of next month
                        next_first_date = next_date + relativedelta(day=1,
                                                                    months=1)
                        next_date = (next_first_date
                                     + relativedelta(days=line.days - 1))
                    elif line.option == 'last_day_following_month':
                        # Getting last day of next month
                        next_date += relativedelta(day=31, months=1)
                    elif line.option == 'last_day_current_month':
                        # Getting last day of next month
                        next_date += relativedelta(day=31, months=0)
                    result.append((fields.Date.to_string(next_date), amt))
                    amount -= amt
            amount = reduce(lambda x, y: x + y[1], result, 0.0)
            dist = round(value - amount, prec)
            if dist:
                last_date = result[-1][0] if result else fields.Date.today()
                result.append((last_date, dist))
        return result
