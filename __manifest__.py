# -*- coding: utf-8 -*-
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

{
    'name': 'Account Payment Schedule',
    'version': '10.0.1.0.O',
    'summary': 'Send Invoices and Track Payments',
    'author': 'LE FILAMENT',
    'license': 'AGPL-3',
    'description': """
    Invoicing & Payments
    ====================
    """,
    'category': 'Accounting',
    'website': 'https://le-filament.com',
    'contributors': [
        'Benjamin Rivier <benjamin@le-filament.com>',
    ],
    'depends': ['account', ],
    'data': [
        'views/account_payment_term.xml',
        'templates/report_invoice.xml'
    ],
    'demo': [],
    'qweb': [],
    'installable': True,
    'application': False,
    'auto_install': False,
}
