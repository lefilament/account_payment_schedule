.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl
   :alt: License: AGPL-3


====================
Account Payment Term
====================

Description
===========

Ce module permet de générer les échéanciers de paiement et de les afficher sur les factures

Credits
=======

Contributors ------------

* Benjamin Rivier <benjamin@le-filament.com>

Maintainer ----------

.. image:: https://le-filament.com/img/logo-lefilament.png
   :alt: Le Filament
   :target: https://le-filament.com

This module is maintained by Le Filament
